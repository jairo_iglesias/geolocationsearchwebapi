
const fs = require('fs');
const CsvReadableStream = require('csv-reader');


function parseNomeLojas(callback){
    
    let inputStream2 = fs.createReadStream('./nome-lojas-sanitize.csv', 'utf8');
    
    let csvStream2 = new CsvReadableStream({
        parseNumbers: true,
        parseBooleans: true,
        trim: true 
    })
    
    const allRecordsLatLong = []
    
    inputStream2
        .pipe(csvStream2)
        .on('data', function (line) {
    
            const lineSplit = line[0].split(';')
            // console.log(lineSplit)
    
            // if(lineSplit[0] == 'End:'){
                const currentRecord = {}
                currentRecord['cnpj'] = lineSplit[0]
                currentRecord['latitude'] = lineSplit[1]
                currentRecord['longitude'] = lineSplit[2]
                // console.log(currentRecord)
                allRecordsLatLong.push(currentRecord)
            // // console.log(lineSplit)
            // // console.log('=====')
            // }
    
        })
        .on('end', function (data) {
            console.log('No more rows!');
            // console.log(allRecordsLatLong)
            callback(allRecordsLatLong)
        });


}


// parseLatLong((allRecordsLatLong) => {

//     let inputStream = fs.createReadStream('./enderecos.csv', 'utf8');
    
//     let csvStream = new CsvReadableStream({
//         parseNumbers: true,
//         parseBooleans: true,
//         trim: true 
//     })
    
//     let allRecords = []
//     let currentRecord = {}
    
//     inputStream
//     	.pipe(csvStream)
//     	.on('data', function (line) {
    
//             const lineSplit = line[0].split(';')
//             // console.log(lineSplit)

//             if(lineSplit[0].substr(0, 2) == 'Lj'){

//                 currentRecord['Loja'] = lineSplit[0]
//                 currentRecord['Nome'] = lineSplit[1]
                
//             }     

//             if(lineSplit[0] == 'CNPJ'){

//                 const cnpj = lineSplit[1]

//                 // Get latitude and longitude this cnpj
//                 const latLong = allRecordsLatLong.filter(item => item.cnpj == cnpj)

//                 if(latLong.length > 0){
//                     const {latitude, longitude} = latLong[0]
//                     currentRecord['latitude'] = latitude
//                     currentRecord['longitude'] = longitude
//                 }

//                 currentRecord['cnpj'] = cnpj
//                 allRecords.push(currentRecord)
//                 currentRecord = {}
//             }

//             if(lineSplit[0] == 'End:'){
//                 currentRecord['endereco'] = lineSplit[1]
//                 currentRecord['numero'] = lineSplit[3]
//                 currentRecord['bairro'] = lineSplit[5]
//                 currentRecord['municipio'] = lineSplit[7]
//                 currentRecord['uf'] = lineSplit[9]
//                 // console.log(currentRecord)
//             // console.log(lineSplit)
//             // console.log('=====')
//             }
//         })
//         .on('end', function (data) {
//             console.log('No more rows!');
//             console.log(allRecords)

//             let jsonSTR = JSON.stringify(allRecords);
//             fs.writeFileSync('placesv3.json', jsonSTR);




//             // allRecords.forEach(item => {
                
//             //     let data = JSON.stringify(item);
//             //     fs.writeFileSync('placesv3.json', data);

//             // })

//         });
// })


const places = require('./placesv3.json')
           
// parseNomeLojas(rows => {
    // console.log(rows)

    // console.log(places)

    let currentRecord = {}
    let allRecords = []

    let inputStream = fs.createReadStream('./nome-lojas-sanitize.csv', 'utf8');
    
    let csvStream = new CsvReadableStream({
        parseNumbers: true,
        parseBooleans: true,
        trim: true 
    })

    inputStream
    	.pipe(csvStream)
    	.on('data', function (line) {

            // console.log('PIPE', line)

            // let jsonSTR = JSON.stringify(allRecords);
            // fs.writeFileSync('placesv3.json', jsonSTR);


            const lineSplit = line[0].split(';')
            // console.log(lineSplit)

            if(lineSplit[0].substr(0, 2) == 'Lj' || lineSplit[1].substr(0, 2) == 'Lj'){

                currentRecord['Loja'] = lineSplit[1]
                currentRecord['Nome'] = lineSplit[2]

                // console.log(currentRecord)
                
            }

            if(lineSplit[1] == 'CNPJ'){

                let cnpj = lineSplit[2]
                cnpj = cnpj.replace(/[^0-9]/g, '')

                currentRecord['cnpj'] = cnpj
                allRecords.push(currentRecord)
                currentRecord = {}

            }

        })
        .on('end', function (data) {

            console.log('No more rows!');
            // console.log(allRecords)

            const placesNew = places.map(place => {
                // console.log('place', place)
                const placeFiltered = allRecords.filter(item => item.cnpj == place.cnpj)
                
                if(placeFiltered.length == 0){
                    return place    
                }
                else{
                    return{
                        ...place,
                        Nome: placeFiltered[0].Nome,
                        Nome_Original: place.Nome
                        
                    }
                }
            })

            console.log(placesNew)


            let newData = JSON.stringify(placesNew);
            fs.writeFileSync('placesv3New.json', newData);


            // let jsonSTR = JSON.stringify(allRecords);
            // fs.writeFileSync('placesv3.json', jsonSTR);


            // allRecords.forEach(item => {
                
            //     let data = JSON.stringify(item);
            //     fs.writeFileSync('placesv3.json', data);

            // })

        });

// })