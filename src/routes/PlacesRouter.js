const express = require('express');
const router = express.Router();
const axios = require('axios')

const places = require('../../placesv3.json')
const closestLocationV2 = require('../../lib/closestLocation')
const geocoding = require('./../../lib/geocoding')

const API_KEY_CEP_ABERTO = 'cbef803324bd262396b9e75270fa2e3a'

const fetchAddressViaCepAPI = async (cep) => {
    
    const URL = `https://viacep.com.br/ws/${cep}/json`
    const response = await axios.get(URL)
    const {data} = response

    return data

}

const fetchAddressCepAbertoAPI = async (cep) => {
    
    const URL = `http://www.cepaberto.com/api/v3/cep?cep=${cep}`
    const response = await axios.get(URL, {
        headers: {
            'Authorization' : `Token token=${API_KEY_CEP_ABERTO}`
        }
    })
    const {data} = response

    return data


}

router.get('/test', async (req, res) => {

    res.status(200).send(process.env.GOOGLE_MAPS_GEOCODING_API_KEY)

})

router.get('/placesByCep/:entity/:cep', async (req, res) => {

    try{
        
        const {entity, cep} = req.params

        console.log(entity, cep)
    
        // const addressData = await fetchAddressCepAbertoAPI(cep)
        // const {latitude, longitude} = addressData
    
        const latlng = await geocoding.fetchLatLong(cep)
    
        console.log('LatLng', latlng)
        console.log('#####')
    
        const closest = closestLocationV2(entity, latlng, places, 3);
    
        // console.log(closest)
    
        res.status(200).send(closest)
    }
    catch(error){
        console.log(error)
        res.status(400).send(error)
    }


})

module.exports = router;